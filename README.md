# Overview
This terraform configuration was used to create an AWS Virtual Private Cloud with an RDS PostgreSQL database. While I used this terraform code to create the database for my eCommerce web app flask-wineshop, it may be used to create an AWS cloud-native PostgreSQL database for any purpose.



